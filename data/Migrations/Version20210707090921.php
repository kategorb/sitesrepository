<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210707090921 extends AbstractMigration
{
    public function getDescription() : string
    {
        $description = 'This is the initial migration which creates blog tables.';
        return $description;
    }

    public function up(Schema $schema) : void
    {
        $table = $schema->createTable('bank_account');
        $table->addColumn('id', 'integer', ['notnull'=>true]);
        $table->addColumn('owner_name', 'string', ['notnull'=>true]);
        $table->setPrimaryKey(['id']);
        $table->addOption('engine' , 'InnoDB');

    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable('bank_account');
        // this down() migration is auto-generated, please modify it to your needs

    }
}

<?php

declare(strict_types=1);

namespace Migrations;

use Application\Entity\Bank_Account;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210706093348 extends AbstractMigration
{
    public function getDescription() : string
    {
        $description = 'This is the initial migration which creates blog tables.';
        return $description;
    }

    public function up(Schema $schema) : void
    {
        $table = $schema->createTable('payment');
        $table->addColumn('id', 'integer', ['autoincrement'=>true]);
        $table->addColumn('sum', 'integer', ['notnull'=>true]);
        $table->addColumn('created_date', 'datetime', ['notnull'=>true]);
        $table->setPrimaryKey(['id']);
        $table->addOption('engine' , 'InnoDB');

    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable('payment');
        // this down() migration is auto-generated, please modify it to your needs

    }
}

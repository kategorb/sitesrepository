<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210707092627 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $table = $schema->getTable('payment');
        $table->addColumn('ba_id', 'integer', ['notnull'=>true]);
        $table->addForeignKeyConstraint('bank_account', ['ba_id'], ['id'], [], 'bank_id_acc');
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function down(Schema $schema) : void
    {
        $table = $schema->getTable('payment');
        $table->removeForeignKey('bank_id_acc');
        // this down() migration is auto-generated, please modify it to your needs

    }
}

<?php
namespace Application\Controller;

use Application\Application\Services\BillCreator;
use Application\Application\Services\Holder;
use Application\Application\Services\MoneyAdder;
use Application\Application\Services\Voider;
use Application\Application\Services\Withdrawer;
use Application\Exception\BankAccountIdNotFoundException;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\ServiceManager\ServiceManager;


class PaymentController extends AbstractActionController
{
    private $serviceManager;

    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }
    public function createAction()
    {
        $createPaymentService = $this->serviceManager->getServiceLocator()->get((BillCreator::class));
        $bankAccountId = $this->params()->fromQuery('ba_id');
        $sum= $this->params()->fromQuery('sum', 100);
        try {
            echo $createPaymentService->create($sum, $bankAccountId);
        } catch (BankAccountIdNotFoundException $e) {
            echo 'Ошибка ввода: ', $e->getMessage(), '<br>';
        }
    }
    public function holdAction()
    {
        $someService = $this->serviceManager->getServiceLocator()->get((Holder::class));
        echo $someService->hold();
    }
    public function addAction()
    {
        $someService = $this->serviceManager->getServiceLocator()->get((MoneyAdder::class));
        echo $someService->adder();
    }
    public function voidAction()
    {
        $someService = $this->serviceManager->getServiceLocator()->get((Voider::class));
        echo $someService->voider();
    }
    public function withdrawAction()
    {
        $someService = $this->serviceManager->getServiceLocator()->get((Withdrawer::class));
        echo $someService->withdraw();
    }

}
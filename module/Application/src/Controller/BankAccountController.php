<?php
namespace Application\Controller;

use Application\Application\Services\AccountChecker;
use Application\Application\Services\AccountCreator;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\ServiceManager\ServiceManager;

class BankAccountController extends AbstractActionController
{
    private $serviceManager;

    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function createAction()
    {
        $bankAccountService = $this->serviceManager->getServiceLocator()->get((AccountCreator::class));
        $id = $this->params()->fromQuery('id', 0000);
        $owner = $this->params()->fromQuery('owner_name', 'root');
        echo $bankAccountService->create($id, $owner);
    }
    public function checkAction()
    {
        $bankAccountService = $this->serviceManager->getServiceLocator()->get((AccountChecker::class));
        echo $bankAccountService->accountChecker();



    }
}
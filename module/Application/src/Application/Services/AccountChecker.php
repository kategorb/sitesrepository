<?php

namespace Application\Application\Services;

use Application\Repository\BankAccountRepository;

class AccountChecker {

    private $bankAccountRepository;
    public $resultStr;

    public function __construct(BankAccountRepository $bankAccountRepository){
        $this->bankAccountRepository = $bankAccountRepository;
        $this->resultStr = '';
    }

    public function accountChecker(){
        $amount = $this->bankAccountRepository->accountBalance();
        foreach ($amount as $value ) {
            $this->resultStr .= $value['id'] . ': '. $value['sum'] . '<br>';
        }
        return $this->resultStr;
    }
}

<?php

namespace Application\Application\Services;

use Application\Entity\Payment;
use Application\Repository\PaymentRepository;
use Application\Exception\BankAccountIdNotFoundException;

class BillCreator {

    private $paymentRepository;

    public function __construct(PaymentRepository $paymentRepository){
        $this->paymentRepository = $paymentRepository;
    }

    public function create($sum, $baId){
        $this->checkingBaId($baId);
        $currentDate = date_create(date('Y-m-d H:i:s'));
        $payment = new Payment($sum, $currentDate, $baId);
        $this->paymentRepository->save($payment);
        return "Saved new product with id " . $payment->getId();
    }

    /**
     * @throws BankAccountIdNotFoundException
     */
    public function checkingBaId($baId)
    {
        $result = $this->paymentRepository->findBaId();
        foreach ($result as $key => $value ) {
            $idArray[$key]= $value['id'];
        }
        if (!in_array($baId, $idArray)) {
            throw new BankAccountIdNotFoundException('id' . $baId . ' не найден ');
        }

    }

}


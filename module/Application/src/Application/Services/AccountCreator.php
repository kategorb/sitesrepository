<?php

namespace Application\Application\Services;

use Application\Entity\BankAccount;
use Application\Repository\BankAccountRepository;

class AccountCreator {

    private $bankAccountRepository;

    public function __construct(BankAccountRepository $bankAccountRepository){
        $this->bankAccountRepository = $bankAccountRepository;
    }

    public function create($id, $ownerName){
        $account = new BankAccount($id, $ownerName);
        $this->bankAccountRepository->save($account);
        return "Saved new account with id ".$id;

    }
}
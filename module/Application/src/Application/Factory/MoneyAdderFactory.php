<?php
namespace Application\Application\Factory;

use Application\Application\Services\MoneyAdder;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class MoneyAdderFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new MoneyAdder();

        return $service;
    }
}
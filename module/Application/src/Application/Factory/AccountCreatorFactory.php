<?php
namespace Application\Application\Factory;

use Application\Application\Services\AccountCreator;
use Application\Repository\BankAccountRepository;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\ServiceManager\ServiceManager;

class AccountCreatorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $serviceManager = $container->get(ServiceManager::class);
        $accountRep = $serviceManager->get(BankAccountRepository::class);
        return new AccountCreator($accountRep);
    }
}
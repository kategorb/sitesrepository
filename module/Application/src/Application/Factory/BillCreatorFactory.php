<?php
namespace Application\Application\Factory;

use Application\Repository\PaymentRepository;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Application\Services\BillCreator;
use Zend\ServiceManager\ServiceManager;

class BillCreatorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $serviceManager = $container->get(ServiceManager::class);
        $paymentRep = $serviceManager->get(PaymentRepository::class);
        return new BillCreator($paymentRep);
    }
}

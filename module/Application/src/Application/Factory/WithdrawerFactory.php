<?php
namespace Application\Application\Factory;

use Application\Application\Services\Withdrawer;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class WithdrawerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        // Создание экземпляра класса.
        $service = new Withdrawer();

        return $service;
    }
}
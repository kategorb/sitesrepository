<?php
namespace Application\Application\Factory;

use Application\Application\Services\Holder;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class HolderFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new Holder();

        return $service;
    }
}
<?php
namespace Application\Application\Factory;

use Application\Application\Services\Voider;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class VoiderFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new Voider();

        return $service;
    }
}
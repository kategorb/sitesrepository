<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Application\Repository\PaymentRepository")
 * @ORM\Table(name="payment")
 */
class Payment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    protected $id;

    /**
     * @ORM\Column(name="sum", type="integer")
     */
    protected $sum;

    /**
     * @ORM\Column(name="created_date", type="datetime")
     */
    protected $createdDate;
    /**
     * @ORM\Column(name="ba_id", type="integer")
     */
    protected $baId;


    public function __construct($sum, $createdDate, $baId) {
        $this->sum = $sum;
        $this->createdDate = $createdDate;
        $this->baId = $baId;
    }
    public function getId(){
        return $this->id;
    }
}

<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Application\Repository\BankAccountRepository")
 * @ORM\Table(name="bank_account")
 */
class BankAccount
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Payment", mappedBy="ba_id")
     * @ORM\Column(name="owner_name", type="string")
     */
    protected $ownerName;


    public function __construct($id, $ownerName) {
        $this->ownerName = $ownerName;
        $this->id = $id;
    }
}
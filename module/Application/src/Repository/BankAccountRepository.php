<?php
namespace Application\Repository;

use Application\Entity\BankAccount;
use Aura\SqlQuery\QueryFactory;
use Doctrine\ORM\EntityManagerInterface;
use PDO;

class BankAccountRepository {

    private $em, $pdo;

    public function __construct(EntityManagerInterface $em, PDO $pdo)
    {
        $this->em = $em;
        $this->pdo = $pdo;
    }

    public function save(BankAccount $account)
    {
        $this->em->persist($account);
        $this->em->flush();
    }
    public function accountBalance()
    {
        $queryFactory = new QueryFactory('pgsql');
        $select = $queryFactory->newSelect();
        $select->cols(['SUM(p.sum)', 'ba.id'])
            ->from('payment AS p')
            ->from('bank_account AS ba')
            ->where('p.ba_id = ba.id')
            ->groupBy(['ba.id']);
        $request = $this->pdo->prepare($select->getStatement());
        $request->execute($select->getBindValues());
        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

}
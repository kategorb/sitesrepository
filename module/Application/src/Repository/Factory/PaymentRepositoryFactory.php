<?php
namespace Application\Repository\Factory;

use Application\Repository\PaymentRepository;
use Interop\Container\ContainerInterface;
use PDO;
use Zend\ServiceManager\Factory\FactoryInterface;

class PaymentRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $paymentManager = $container->get('doctrine.entitymanager.orm_default');
        $pdo = new PDO('pgsql:host=127.0.0.1;dbname=payment_db', 'kate', '1q2w3e4r');
        return new PaymentRepository($paymentManager, $pdo);

    }
}
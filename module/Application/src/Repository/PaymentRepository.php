<?php
namespace Application\Repository;

use Application\Entity\BankAccount;
use Aura\SqlQuery\QueryFactory;
use Doctrine\ORM\EntityManagerInterface;
use Application\Entity\Payment;
use Doctrine\ORM\Mapping;
use PDO;

class PaymentRepository {

    private $em;

    public function __construct(EntityManagerInterface $em, PDO $pdo)
    {
        $this->em = $em;
        $this->pdo = $pdo;
    }


    public function save(Payment $payment)
    {
        $this->em->persist($payment);
        $this->em->flush();
    }

    public function findBaId()
    {
        $queryFactory = new QueryFactory('pgsql');
        $select = $queryFactory->newSelect();
        $select->cols(['ba.id'])
            ->from('bank_account AS ba')
            ->groupBy(['ba.id']);
        $request = $this->pdo->prepare($select->getStatement());
        $request->execute($select->getBindValues());
        return $request->fetchAll(PDO::FETCH_ASSOC);
    }
}

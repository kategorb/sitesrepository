<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Application\Factory\AccountCheckerFactory;
use Application\Application\Factory\AccountCreatorFactory;
use Application\Application\Factory\BillCreatorFactory;
use Application\Application\Factory\HolderFactory;
use Application\Application\Factory\MoneyAdderFactory;
use Application\Application\Factory\VoiderFactory;
use Application\Application\Factory\WithdrawerFactory;
use Application\Application\Services\AccountChecker;
use Application\Application\Services\AccountCreator;
use Application\Application\Services\BillCreator;
use Application\Application\Services\Holder;
use Application\Application\Services\MoneyAdder;
use Application\Application\Services\Voider;
use Application\Application\Services\Withdrawer;
use Application\Controller\Factory\BankAccountControllerFactory;
use Application\Controller\Factory\PaymentControllerFactory;
use Application\Repository\BankAccountRepository;
use Application\Repository\Factory\BankAccountRepositoryFactory;
use Application\Repository\Factory\PaymentRepositoryFactory;
use Application\Repository\PaymentRepository;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Doctrine\DBAL\Driver\PDO\PgSql\Driver as PDOPgSqlDriver;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'payment' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/payment[/:action]',
                    'defaults' => [
                        'controller' => Controller\PaymentController::class,
                        'action'     => 'create',
                    ],
                ],
            ],
            'bank_account' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/bank_account[/:action]',
                    'defaults' => [
                        'controller' => Controller\BankAccountController::class,
                        'action'     => 'create',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
            Controller\PaymentController::class => Controller\Factory\PaymentControllerFactory::class,
            Controller\BankAccountController::class => Controller\Factory\BankAccountControllerFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'service_manager' => [
        'factories' => [
            BillCreator::class => BillCreatorFactory::class,
            Holder::class => HolderFactory::class,
            MoneyAdder::class => MoneyAdderFactory::class,
            Voider::class => VoiderFactory::class,
            Withdrawer::class => WithdrawerFactory::class,
            AccountCreator::class => AccountCreatorFactory::class,
            AccountChecker::class => AccountCheckerFactory::class,
            BankAccountRepository::class => BankAccountRepositoryFactory::class,
            PaymentRepository::class => PaymentRepositoryFactory::class,
        ],
    ],
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => PDOPgSqlDriver::class,
                'params' => [
                    'host'     => '127.0.0.1',
                    'user'     => 'kate',
                    'password' => '1q2w3e4r',
                    'dbname'   => 'payment_db',
                ]
            ],
        ],
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
        'migrations_configuration' => [
            'orm_default' => [
                'directory' => 'data/Migrations',
                'name'      => 'Doctrine Database Migrations',
                'namespace' => 'Migrations',
                'table'     => 'migrations',
            ],
        ],
    ],
];

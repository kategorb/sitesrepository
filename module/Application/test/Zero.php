<?php

namespace ApplicationTest;

class Zero {

    public function __construct($num1, $num2) {
        $this->number1 = $num1;
        $this->number2 = $num2;
    }

    public function zeroDiv() {
        return $this->number1/ $this->number2;
    }

}
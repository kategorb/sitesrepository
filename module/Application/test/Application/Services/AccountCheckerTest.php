<?php

namespace ApplicationTest\Application\Services;

use Application\Application\Services\AccountChecker;
use Application\Repository\BankAccountRepository;
use Mockery;
use PHPUnit\Framework\TestCase;

class AccountCheckerTest extends TestCase
{

    private $mockedRep;

    public function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->mockedRep = Mockery::mock(BankAccountRepository::class);
    }

    public function testAccountChecker(){
        $this->mockedRep->shouldReceive('accountBalance')
            ->once()
            ->andReturn( [["sum"=> 6072, "id" => 2002]] );
        $checker = new AccountChecker($this->mockedRep);
        $result = $checker->accountChecker();
        $this->assertEquals('2002: 6072<br>', $result);



    }

}
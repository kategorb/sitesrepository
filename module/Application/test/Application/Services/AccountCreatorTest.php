<?php
namespace ApplicationTest\Application\Services;

use Application\Application\Services\AccountCreator;
use Application\Entity\BankAccount;
use Application\Repository\BankAccountRepository;
use Mockery;
use PHPUnit\Framework\TestCase;

class AccountCreatorTest extends TestCase {

    private $mockedRep;

    public function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->mockedRep = Mockery::mock(BankAccountRepository::class);
    }

    public function testAccountCreator(){
        $this->mockedRep->shouldReceive('save')
            ->with(BankAccount::class)
            ->once();
        $creator = new AccountCreator($this->mockedRep);
        $result = $creator->create(1500, 'bird');
        $this->assertEquals("Saved new account with id 1500", $result);


    }

}
